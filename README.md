# Welcome to ScanMe Apps!

Halo Saya Fransiscus Pandhu menyerahkan script code dan juga APK build dalam repository ini. Adapun langkah-langkah untuk membuild script dan langkah penggunaan aplikasi.

## Langkah Build

 1. open terminal cd ke root repository
 2. cd ke project -> MobDevFransiscusPandhu
 3. npm install
 4. react-native link react-native-svg
 5. react-native link react-native-camera
 6. react-native link react-native-qrcode-scanner
 7. react-native link react-native-permissions
 8. react-native run-android untuk mencoba di Android

## Data Penggunaan Aplikasi
Terdapat 2 user yang bisa digunakan :

> Email : codemi.1@gmail.com
> Pass : Codemi1
> 
> Email : codemi.2@gmail.com
> Pass: Codemi2
