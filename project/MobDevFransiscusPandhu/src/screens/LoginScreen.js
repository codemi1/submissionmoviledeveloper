import React, { useState } from 'react'
import DotBackground from '../components/DotBackground'
import Logo from '../components/Logo'
import HeaderText from '../components/HeaderText'
import Button from '../components/Button'
import TextInput from '../components/TextInput'
import { emailValidator } from '../utilities/emailValidator'
import { passwordValidator } from '../utilities/passwordValidator'
import Toast from '../components/Toast'
import { loginUser } from '../api/auth-service'

const LoginScreen = ({ navigation }) => {
  const [email, setEmail] = useState({ value: '', error: '' })
  const [password, setPassword] = useState({ value: '', error: '' })
  const [loading, setLoading] = useState()
  const [error, setError] = useState()

  const onLoginPressed = async () => {
    const emailError = emailValidator(email.value)
    const passwordError = passwordValidator(password.value)
    if (emailError || passwordError) {
      setEmail({ ...email, error: emailError })
      setPassword({ ...password, error: passwordError })
      return
    }
    setLoading(true)
    const response = await loginUser({
      email: email.value,
      password: password.value,
    })
    if (response.error) {
      setError(response.error)
    } else {
      navigation.replace('Main')
    }
    setLoading(false)
  }

  return (
    <DotBackground>
      <Logo />
      <HeaderText>Scan Your Friend</HeaderText>
      <TextInput
        label="Email"
        returnKeyType="next"
        value={email.value}
        onChangeText={(text) => setEmail({ value: text, error: '' })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      />
      <TextInput
        label="Password"
        returnKeyType="done"
        value={password.value}
        onChangeText={(text) => setPassword({ value: text, error: '' })}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />
      <Button loading={loading} mode="contained" onPress={onLoginPressed}>
        Login
      </Button>
      <Toast message={error} onDismiss={() => setError('')} />
    </DotBackground>
  )
}

export default LoginScreen