import React, { useState } from 'react';

import {
  AppRegistry,
  Text,
  TouchableOpacity,
  View
} from 'react-native';

import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import { styles } from '../styles/HomeScreenStyles'
import Toast from '../components/Toast'
import SuccessToast from '../components/SuccessToast'
import { getData } from '../api/user-service'

const HomeScreen = ( ) => {
  const [msg, setResponse] = useState()
  const [error, setError] = useState()

  const onSuccess = async (e) => {
    const response = await getData({
      key: e.data,
    })
    if (response.error) {
      setError(response.error)
    } else {
      setResponse('Scan Success With Data ' + response.user.val().name)
    }
  }

  return (
    <View>
      <QRCodeScanner
          onRead={onSuccess}
          reactivate={true}
          showMarker={true}
          flashMode={RNCamera.Constants.FlashMode.off}
          topContent={
            <Text style={styles.centerText}>
            </Text>
          }
        />
      <SuccessToast message={msg} onDismiss={() => setResponse('')} />
      <Toast message={error} onDismiss={() => setError('')} />
    </View>
  )
}

export default HomeScreen
