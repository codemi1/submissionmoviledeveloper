import React from 'react'
import { ActivityIndicator } from 'react-native'
import DotBackground from '../components/DotBackground'
import { theme } from '../styles/Theme'
import auth from "@react-native-firebase/auth"



const LoadingScreen = ({ navigation }) => {
    auth().onAuthStateChanged((user) => {
        if (user) {
            // User is logged in
            navigation.reset({
            index: 0,
            routes: [{ name: 'Main' }],
            })
        } else {
            // User is not logged in
            navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
            })
        }
        })

    return (
        <DotBackground>
            <ActivityIndicator size="large" color={theme.colors.primary} />
        </DotBackground>
    )
}

export default LoadingScreen