import React, { useState } from 'react';
import DotBackground from '../components/DotBackground'
import HeaderText from '../components/HeaderText'
import LogOutButton from '../components/LogOutButton'
import { logoutUser } from '../api/auth-service'
import QRCode from 'react-native-qrcode-svg';
import auth from "@react-native-firebase/auth"

const ProfileScreen = () => {
  let logoFromFile = require('../assets/logo.png');
  const [key, setUser] = useState()

  auth().onAuthStateChanged((user) => {
      if (user) {
          // User is logged in
          setUser(user.uid)
      } 
      })

  return (
    <DotBackground>
      <LogOutButton goBack={logoutUser}/>
      <QRCode
        value={key}
        logo={logoFromFile}
        size={200}
      />
      <HeaderText>Scan Above to Know My Name</HeaderText>
    </DotBackground>
  )
}

export default ProfileScreen
