import React from 'react'
import { Snackbar } from 'react-native-paper'
import { StyleSheet, View, Text } from 'react-native'
import { getStatusBarHeight } from 'react-native-status-bar-height'
import { theme } from '../styles/Theme'

const SuccessToast = ({ message, onDismiss }) => (
  <View style={styles.container}>
    <Snackbar
      visible={!!message}
      duration={3000}
      onDismiss={onDismiss}
      style={{
        backgroundColor: theme.colors.primary,
      }}
    >
      <Text style={styles.content}>{message}</Text>
    </Snackbar>
  </View>
)

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 80 + getStatusBarHeight(),
    width: '100%',
  },
  content: {
    fontWeight: '900',
  },
})

export default SuccessToast
