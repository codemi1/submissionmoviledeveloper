import { DefaultTheme } from 'react-native-paper'

export const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    text: '#000000',
    textWhite: '#ffffff',
    primary: '#04acfc',
    secondary: '#414757',
    error: '#f13a59',
    grey: '#95a5a6'
  },
}