/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react'
import { Provider } from 'react-native-paper'

import { NavigationContainer } from '@react-navigation/native';
import AuthNavigation from './navigations/AuthNavigations'
import { theme } from './styles/Theme'

const App = () => {
  return (
    <Provider theme={theme}>
      <NavigationContainer>
        <AuthNavigation/>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
