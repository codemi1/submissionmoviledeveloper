import auth from "@react-native-firebase/auth"


export const getUserKey = () => {
  auth().onAuthStateChanged((user) => {
    if (user) {
        // User is logged in
        return user.uid
    } 
    })
}

export const logoutUser = () => {
  auth().signOut()
}

export const loginUser = async ({ email, password }) => {
  try {
    const user = await auth()
      .signInWithEmailAndPassword(email, password)
    return { user }
  } catch (error) {
    return {
      error: error.message,
    }
  }
}

export const checkAuth = async () => {
  try {
    auth().onAuthStateChanged((user) => {
      if (user) {
        // User is logged in
        return {
          status: true,
          message: 'You are Logged In',
        }
      } else {
        // User is not logged in
        return {
          status: false,
          message: 'You are not Logged into the System',
        }
      }
    })
  } catch (error) {
    return {
      status: false,
      message: error.message,
    }
  }
}