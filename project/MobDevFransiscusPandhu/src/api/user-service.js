import database from '@react-native-firebase/database';

export const getData = async ({ key }) => {
  try {
    const user = await database()
        .ref('/users/' + key)
        .once('value')
    return { user }
  } catch (error) {
    return {
      error: error.message,
    }
  }
}