import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from '../screens/HomeScreen'
import ProfileScreen from '../screens/ProfileScreen'
import Icon from 'react-native-vector-icons/Ionicons';
import { theme } from '../styles/Theme'


const Tab = createBottomTabNavigator();

function MainNavigation() {
    return (
        <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ color, size }) => {
            let iconName;

            if (route.name === 'Scan Now!') {
              iconName = 'qr-code-outline';
            } else if (route.name === 'Profile') {
              iconName = 'people-outline';
            }

            // You can return any component that you like here!
            return <Icon name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
            activeTintColor: theme.colors.primary,
            inactiveTintColor: 'gray',
            showIcon: true
        }}
        >
        <Tab.Screen name="Scan Now!" component={HomeScreen} />
        <Tab.Screen name="Profile" component={ProfileScreen} />
      </Tab.Navigator>
    );
  }
  
  export default MainNavigation;