import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../screens/LoginScreen'
import MainNavigation from './MainNavigations'
import LoadingScreen from '../screens/LoadingScreen'

const Stack = createStackNavigator();

function AuthNavigation() {
  return (
    <Stack.Navigator
      initialRouteName="Loading">
      <Stack.Screen 
        name="Loading" 
        component={LoadingScreen}
        options={{ headerShown: false }} />
      <Stack.Screen 
        name="Login" 
        component={LoginScreen}
        options={{ headerShown: false }} />
      <Stack.Screen 
        name="Main"
        component={MainNavigation}
        options={{ headerShown: false }} />
    </Stack.Navigator>
  );
}

export default AuthNavigation;